#      	      GUIA PARA CONFIGURAR ESP8266 V.01 EN ARDUINO                    
[programarfacil](https://programarfacil.com/podcast/como-configurar-esp01-wifi-esp8266/)

INFORMACIÓN:
================================================================================
* GND es la toma de tierra.
* GPIO2 es una entrada salida de propósito general. Es el pin digital número 2.
* GPIO0 es una entrada salida de propósito general. Es el pin digital número 0.
* RXD es el pin por donde se van a recibir los datos del puerto serie. Trabaja 
a 3,3 V. También se puede utilizar como pin digital GPIO: sería el número 3.
* TXD es el pin por donde se van a transmitir los datos del puerto serie. 
Trabaja a 3,3 V. También se puede utilizar como pin digital GPIO: sería el número 1.
* CH_PD pin para apagar y encender el ESP-01: si lo ponemos a 0 V (LOW) se apaga, 
y a 3,3 V (HIGH) se enciende.
* RESET pin para resetear el ESP-01: si lo ponemos a 0 V (LOW) se resetea.
* Vcc es por donde alimentamos el ESP-01. Funciona a 3,3 V y admite un máximo de 
3,6 V. La corriente suministrada debe ser mayor que 200 mA.


ALIMENTACIÓN:
================================================================================
Antes de hacer nada, tenemos que alimentar el ESP-01: funciona a una tensión de 
3,3 V y admite un máximo de 3,6 V. Se recomienda una intensidad mayor de 200 mA 
debido a que cuando está transmitiendo a través de WiFi puede alcanzar picos de 
consumo de más de 200 mA.

PARÁMETROS DE CONFIGURACIÓN
===============================================================================
Flash Mode → «DIO»

Flash Frequency → «40MHz»

CPU Frequency → «80MHz»

Flash Size → «512K (64K SPIFFS)»

Debug port → «Disabled»

Debug Level → «Ninguno»

Reset Method → «ck»

Upload Speed → «115200»

