/*=========================================================================================================================================================*/
/*=============================================================BIBLIOTECAS=================================================================================*/
/*=========================================================================================================================================================*/
#include <ESP8266WiFi.h> 
#include <ArduinoJson.h>
#include "coap_client.h"
#include "DHT.h"

/*
  https://github.com/automote/ESP-CoAP
*/
/*=========================================================================================================================================================*/
/*=============================================================DECLARACIONES===============================================================================*/
/*=========================================================================================================================================================*/

// coap ==> INstancia del cliente CoAP
coapClient coap;

// ssid, password, DEVICE_SECRET_KEY ==> Datos de la conexión Wifi
const char* ssid = "MOVISTAR_CD10";
const char* password = "HtBxUm7JKUQqRrMq5s4D";
String DEVICE_SECRET_KEY  = "your-device_secret_key";

// ip, port, path ==> Direción Ip y puerto por defecto CoAP (5683) con el Path CoAP empleado
IPAddress ip(192, 168, 1, 39);
int port = 5683;
int tiempo = 30000; //30 segundos por defecto

StaticJsonBuffer<200> jsonBuffer;
JsonObject& root = jsonBuffer.createObject();

// Definiendo el modelo de sensor y el pin al que estará conectado 
#define DHTTYPE DHT11 //DHT21, DHT22
#define DHTPIN  2 // GPIO2

DHT dht(DHTPIN, DHTTYPE, 27); // 11 works fine for ESP8266 threshold => MHZ CPU

/*** Variables para Humedad y Temperatura ****/
float temperatura; // double
float humedad;
/*=========================================================================================================================================================*/
/*=============================================================COAP CALLBACK===============================================================================*/
/*=========================================================================================================================================================*/

// callback_response ==> Callback de respuesta del cliente CoAP
void callback_response(coapPacket &packet, IPAddress ip, int port) {
  char p[packet.payloadlen + 1];
  memcpy(p, packet.payload, packet.payloadlen);
  p[packet.payloadlen] = NULL;
  
  //Respuesta del servidor CoAP
  if (packet.type == 3 && packet.code == 0) {
    Serial.println("ping ok");
  }
  
}

/*=========================================================================================================================================================*/
/*=============================================================SETUP=======================================================================================*/
/*=========================================================================================================================================================*/

// setup ==> Código que inicializa el programa
void setup() {
  Serial.begin(115200); //Puerto serie activo a 115200 Hz
  
  WiFi.begin(ssid, password); //Arranca conexión WiFi

  // Información de la conexión WiFi
//  Serial.println();//Salto de línea
//  Serial.println();//Salto de línea
//  Serial.print("Connecting to ");
//  Serial.println(ssid);
  WiFi.begin(ssid, password); //Arranca conexión WiFi
  while (WiFi.status() != WL_CONNECTED) {
    yield(); //Pasa el contról a otras taréas mientras ésta se completa,  se usa en funciones que tardan en completarse.
    //Serial.print(".");
  }//Se ha completado la conexión
//  Serial.println("");
//  Serial.println("WiFi connected");
  // Print the IP address of client
//  Serial.println(WiFi.localIP()); // Ip asignada al equipo

  // Se asigna la función "callback_response" a las respuestas del servidor CoAP
  coap.response(callback_response);

  // Arranca el cliente CoAP
  coap.start();
  
  dht.begin();
}

/*=========================================================================================================================================================*/
/*=============================================================LOOP========================================================================================*/
/*=========================================================================================================================================================*/

void loop() {
  //char* path_1 = "obs";
  //int valor = coap.observe(ip,port,path_1,observe_value); 
  
  temperatura = dht.readTemperature();
  humedad = dht.readHumidity();
   
  root["temperatura"] =  temperatura;
  root["humedad"] =  humedad;
  //root["Observador"] =  valor;
  /*root["accessToken"] =  DEVICE_SECRET_KEY;*/

  String data; // Generar un string en el que plasmar el JSON generado
  root.printTo(data); // Imprimo en la variable string el JSON generado
  char dataChar[data.length() + 1];
  data.toCharArray(dataChar, data.length() + 1); // Copia los caracteres de la cadena (data) en el búfer proporcionado (dataChar)
  bool state;


  // Envía una solicitud POST con la información
  // Pasamos como argumento: ip (del servidor), port (puerto CoAP del servidor), path (de CoAP que estemos empleando), dataChar ()
  char* path = "events";
  int msgid = coap.post(ip, port, path, dataChar, data.length());
  int res = coap.get(ip, port, "delay");   
  Serial.println(res);         
  state = coap.loop();
  
  delay(tiempo); 
}
